import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {TreeComponent} from "./component/tree";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        TreeComponent
      ],
      imports: [
        NgxDatatableModule,
        BrowserModule,
        HttpModule]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'webix-app'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('webix-app');
  });

  it('should render title in a h1 tag', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to webix-app!');
  });
});
