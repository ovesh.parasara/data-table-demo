import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';

import { AppComponent } from './app.component';
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {ListCountryService} from "./services/list-country.service";
import {HttpModule} from "@angular/http";
import {TreeComponent} from "./component/tree";

@NgModule({
  declarations: [
    AppComponent,
    TreeComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    NgxDatatableModule
  ],
  providers: [ListCountryService],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA , NO_ERRORS_SCHEMA ]
})
export class AppModule { }
